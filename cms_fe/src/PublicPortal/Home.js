import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState, useEffect } from 'react';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { Card } from "react-bootstrap";

function Home() {

    const [cardData, setCardData] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/contents/all")
          .then((response) => response.json())
          .then((data) => {
            // Filter the data to only include items with the category "HOME"
            const homeData = data.filter((item) => item.category === 'HOME');
            setCardData(homeData);
          })
          .catch((error) => {
            console.error(error);
          });
      }, []);

    // const handleImageLoad = (event, index) => {
    //     const fileReader = new FileReader();
    //     fileReader.onload = () => {
    //       const dataUrl = fileReader.result;
    //       const base64Image = dataUrl.split(",")[1];
    //       const newData = [...cardData];
    //       newData[index].image = base64Image;
    //       setCardData(newData);
    //     };
    //     fileReader.readAsDataURL(event.target.files[0]);
    //   };

    return ( 
        <>
            <header className="masthead" style={{ backgroundImage: "url('https://static.vecteezy.com/system/resources/previews/004/630/855/large_2x/top-view-blue-pastel-office-desk-with-computer-office-supplies-cell-phone-and-black-coffee-cup-free-photo.jpg')",  backgroundSize: "cover" }}>
                <div className="container position-relative px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div className="col-md-10 col-lg-8 col-xl-7">
                            <div className="text-center">
                                <div className="page-heading">
                                    <h1>Take Control of Your Content, Take Control of Your Future!</h1>
                                    <span className="subheading">Come take a tour on our contents from publishers around the world.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            {/* <div className="timeline">
                {cardData.map(card => (
                    <div key={card.id} className="timeline-item">
                    <div className="timeline-img"></div>
                    <div className="timeline-content">
                        <h2 className="timeline-title">{card.title}</h2>
                        {card.media && <img src={card.media} alt={card.title} />}
                        <p className="card-text">{card.description}</p>
                        <p className="card-text">
                            Last updated at: {card.updatedAt} 
                        </p>
                    </div>
                    </div>
                ))}
            </div> */}

            <div className="row justify-content-center align-items-center">
            {cardData.map((card) => (
                <div key={card.id} className="col-md-10 mb-4">
                <Card className="custom-card">
                    <Card.Header className="font-weight-bolder text-center">
                    {card.title}
                    </Card.Header>
                    <Card.Body className="d-flex flex-column align-items-center">
                    {card.image && (
                        <div className="mb-3">
                        <img
                            src={`data:image/png;base64,${card.image}`}
                            alt="card"
                            className="img-fluid"
                        />
                        </div>
                    )}
                    <div className="text-center">
                        <p className="card-text">{card.description}</p>
                        <p className="card-text">
                        <small className="text-muted">
                            Last updated {card.updatedAt}
                        </small>
                        </p>
                    </div>
                    </Card.Body>
                </Card>
                </div>
            ))}
            </div>

            {/* Footer*/}
            <div className="footer-line">
                <footer className="border-top">
                    <div className="container px-4 px-lg-5">
                            <div class="row gx-4 gx-lg-5 justify-content-center">
                                <div className="container">
                                    <div className="social-icons">
                                        <ul className="list-inline text-center">
                                            <li className="list-inline-item">
                                                <a href="https://www.facebook.com/"><FontAwesomeIcon icon={faFacebook} size="3x"/></a>
                                                <a href="https://twitter.com/"><FontAwesomeIcon icon={faTwitter} size="3x"/></a>
                                                <a href="https://www.instagram.com/"><FontAwesomeIcon icon={faInstagram} size="3x"/></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-10 col-lg-8 col-xl-7">
                                    <br/>
                                    <div className="small text-center text-muted fst-italic">Copyright &copy; Word Push 2023</div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                </footer>
            </div>
            
        </>
     );
}

export default Home;