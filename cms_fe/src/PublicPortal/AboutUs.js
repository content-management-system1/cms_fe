import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { useState, useEffect } from 'react';
import { Card } from "react-bootstrap";

function AboutUs() {

    const [cardData, setCardData] = useState([]);

    useEffect(() => {
        const fetchCardData = async () => {
        const response = await fetch('http://localhost:8080/contents/all');
        const data = await response.json();
        const contactCardData = data.filter(item => item.category === 'ABOUT_US');
        setCardData(contactCardData);
        };

        fetchCardData();
    }, []);
               

    return ( 
        <>
            <header className="masthead" style={{ backgroundImage: "url('https://wallpaperaccess.com/full/695377.jpg')",  backgroundSize: "cover" }}>
                <div className="container position-relative px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div className="col-md-10 col-lg-8 col-xl-7">
                            <div className="text-center">
                                <div className="page-heading">
                                    <h1>About Us</h1>
                                    <span className="subheading">This is what we do.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div className="row justify-content-center align-items-center">
            {cardData.map((card) => (
                <div key={card.id} className="col-md-10 mb-4">
                <Card className="light-blue">
                    <Card.Header className="font-weight-bolder text-center">
                    {card.title}
                    </Card.Header>
                    <Card.Body className="d-flex flex-column align-items-center">
                    {card.image && (
                        <div className="mb-3">
                        <img
                            src={`data:image/png;base64,${card.image ? card.image.toString('base64') : ''}`}
                            alt="Media"
                            className="img-fluid"
                        />
                        </div>
                    )}
                    <div className="text-center">
                        <p className="card-text">{card.description}</p>
                        <p className="card-text">
                        <small className="text-muted">
                            Last updated {card.updatedAt}
                        </small>
                        </p>
                    </div>
                    </Card.Body>
                </Card>
                </div>
            ))}
            </div>

            {/* Footer*/}
            <div className="footer-line">
                <footer className="border-top">
                    <div className="container px-4 px-lg-5">
                            <div class="row gx-4 gx-lg-5 justify-content-center">
                                <div className="container">
                                    <div className="social-icons">
                                        <ul className="list-inline text-center">
                                            <li className="list-inline-item">
                                                <a href="https://www.facebook.com/"><FontAwesomeIcon icon={faFacebook} size="3x"/></a>
                                                <a href="https://twitter.com/"><FontAwesomeIcon icon={faTwitter} size="3x"/></a>
                                                <a href="https://www.instagram.com/"><FontAwesomeIcon icon={faInstagram} size="3x"/></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-10 col-lg-8 col-xl-7">
                                    <br/>
                                    <div className="small text-center text-muted fst-italic">Copyright &copy; Word Push 2023</div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                </footer>
            </div>
            
        </>
     );
}

export default AboutUs;