import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { useState, useEffect } from 'react';

function ContactUs() {


    const [formData, setFormData] = useState({
        name: '',
        email: '',
        phoneNumber: '',
        query: ''
      });

    const handleChange = (event) => {
    setFormData({
        ...formData,
        [event.target.name]: event.target.value
    });
    }
    
    const handleSubmit = (event) => {
        event.preventDefault();

        fetch('http://localhost:8080/add-request', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to submit form data');
            }
            // clear the form data
            setFormData({
                name: '',
                email: '',
                phoneNumber: '',
                query: ''
            });
            // show success message
            alert('Form submitted successfully!');
            })
            .catch(error => {
            console.error(error);
            // show error message
            alert('Failed to submit form data');
            });
        };

    const [cardData, setCardData] = useState([]);

    // useEffect(() => {
    //   const fetchCardData = async () => {
    //     const response = await fetch('http://localhost:8080/contents/all');
    //     const data = await response.json();
    //     const contactCardData = data.filter(item => item.category === 'CONTACT');
    //     setCardData(contactCardData);
    //   };
  
    //   fetchCardData();
    // }, []);
  
    
    
      return (
        <>
          <header className="masthead" style={{ backgroundImage: "url('https://t3.ftcdn.net/jpg/02/99/79/52/360_F_299795280_qfV0uJQmM8FR0lkhCxe5ZRGXUlrwMgV2.webp')" ,  backgroundSize: "cover"}}>
            <div className="container position-relative px-4 px-lg-5">
              <div className="row gx-4 gx-lg-5 justify-content-center">
                <div className="col-md-10 col-lg-8 col-xl-7">
                <div className="text-center">
                    <div className="page-heading">
                        <h1>Contact Us</h1>
                        <span className="subheading">You have questions? We have answers.</span>
                    </div>
                </div>
                </div>
              </div>
            </div>
          </header>
    
          <main className="mb-4">
            <div className="container px-4 px-lg-5">
              <div className="row gx-4 gx-lg-5 justify-content-center">
                <div className="col-md-10 col-lg-8 col-xl-7">
                  <p>Want to get in touch? Fill out the form below to send me a message and we will get back to you as soon as possible!</p>
                  
                  <div className="my-5">
                    <form id="contactForm" onSubmit={handleSubmit}>
                      <div className="form-floating">
                        <input className="form-control" id="name" name="name" type="text" placeholder="Enter your name..." value={formData.name} onChange={handleChange} required />
                        <label htmlFor="name">Name</label>
                      </div>
                      <div className="form-floating">
                        <input className="form-control" id="email" name="email" type="email" placeholder="Enter your email..." value={formData.email} onChange={handleChange} required />
                        <label htmlFor="email">Email address</label>
                      </div>
                      <div className="form-floating">
                        <input className="form-control" id="phoneNumber" name="phoneNumber" type="tel" placeholder="Enter your phone number..." value={formData.phoneNumber} onChange={handleChange} required />
                        <label htmlFor="phoneNumber">Phone Number</label>
                      </div>
                      <div className="form-floating">
                        <textarea className="form-control" id="query" name="query" placeholder="Enter your message here..." style={{ height: "12rem" }} value={formData.query} onChange={handleChange} required></textarea>
                        <label htmlFor="query">Message</label>
                      </div>
                      <br />
                      <button className="btn btn-primary text-uppercase" type="submit">Send</button>
                    </form>
                  </div>
                </div>
                </div>
            </div>
        </main>

        {/* <div className="text-center">
          {cardData.map(card => (
            <div key={card.id} className="card d-inline-block m-3 bg-info">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">{card.title}</h5>
                  <p className="card-text">{card.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div> */}

        {/* Footer*/}
        <div className="footer-line">
                <footer className="border-top">
                    <div className="container px-4 px-lg-5">
                            <div class="row gx-4 gx-lg-5 justify-content-center">
                                <div className="container">
                                    <div className="social-icons">
                                        <ul className="list-inline text-center">
                                            <li className="list-inline-item">
                                                <a href="https://www.facebook.com/"><FontAwesomeIcon icon={faFacebook} size="3x"/></a>
                                                <a href="https://twitter.com/"><FontAwesomeIcon icon={faTwitter} size="3x"/></a>
                                                <a href="https://www.instagram.com/"><FontAwesomeIcon icon={faInstagram} size="3x"/></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-10 col-lg-8 col-xl-7">
                                    <br/>
                                    <div className="small text-center text-muted fst-italic">Copyright &copy; Word Push 2023</div>
                                    <br/>
                                </div>
                            </div>
                        </div>
                </footer>
            </div>
        </>

      );
    }

     

export default ContactUs;