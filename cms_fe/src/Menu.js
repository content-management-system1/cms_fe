import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Menu() {
    return ( 
        <>
            <body> 
                {/* Navigation*/}
                <nav className="navbar navbar-expand-lg navbar-light" id="mainNav">
                    <div className="container px-4 px-lg-5">
                    {/* <FontAwesomeIcon icon={faCircle} /> */}
                    <img src="https://png.pngtree.com/png-vector/20210313/ourmid/pngtree-letter-w-logo-png-png-image_3045051.jpg" alt="Logo" width="30" height="24" class="d-inline-block align-text-top" />
                    <a className="navbar-brand" href="/home">Word Push</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    </button>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav ms-auto py-4 py-lg-0">
                        <li className="nav-item"><a className="nav-link px-lg-3 py-3 py-lg-4" href="/home">Home</a></li>
                        <li className="nav-item"><a className="nav-link px-lg-3 py-3 py-lg-4" href="/about-us">About Us</a></li>
                        <li className="nav-item"><a className="nav-link px-lg-3 py-3 py-lg-4" href="/contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                    </div>
                </nav>
            </body>
        </>
            
     );
}

export default Menu;