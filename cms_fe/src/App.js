import Home from './PublicPortal/Home';
import AboutUs from './PublicPortal/AboutUs';
import ContactUs from './PublicPortal/ContactUs';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Menu from './Menu';
import './App.css';

function App() {
  return (
    <Router>
      <Menu/>
        <Routes><Route path='/' element={<Home />}></Route>
          <Route path='/home' element={<Home />}></Route>
          <Route path='/about-us' element={<AboutUs />}></Route>
          <Route path='/contact-us' element={<ContactUs />}></Route>
        </Routes>
    </Router>
  );
}


export default App;
